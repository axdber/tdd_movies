#RUN parallel tests
docker-compose exec movies pytest -p no:warnings -k "unit" -n auto

#update containers
docker-compose up -d --build

#correct errors
flake8 . && black --exclude=migrations . && isort ./*/*.py

#check errors
flake8 . && black --check --exclude="migrations|env" . && isort ./*/*.py --check-only