from rest_framework.routers import DefaultRouter

from .views import MoviesViewSet

router = DefaultRouter()
router.register(r"api/movies", MoviesViewSet, basename="movie")
urlpatterns = router.urls
