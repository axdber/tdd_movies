from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.response import Response

from .models import Movie
from .serializers import MovieSerializer

# class MoviesViewSet(viewsets.ViewSet):

#     def list(self, request):
#         queryset = Movie.objects.all()
#         serializer = MovieSerializer(queryset, many=True)
#         return Response(serializer.data)

#     def retrieve(self, request, pk=None):
#         queryset = Movie.objects.all()
#         movie = get_object_or_404(queryset, pk)
#         serializer = MovieSerializer(movie)
#         return Response(serializer.data)


class MoviesViewSet(viewsets.ModelViewSet):

    serializer_class = MovieSerializer
    queryset = Movie.objects.all()

    def get_queryset(self):
        return Movie.objects.all()

    def list(self, request):
        serializer = self.get_serializer(self.get_queryset(), many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        try:
            int(pk)
        except ValueError:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        movie = get_object_or_404(self.get_queryset(), id=pk)
        if movie is status.HTTP_404_NOT_FOUND:
            return movie

        serializer = self.get_serializer(movie)

        return Response(serializer.data)

    def destroy(self, request, pk=None):
        movie = get_object_or_404(self.get_queryset(), id=pk)
        print(movie)
        a = movie.delete()
        print(a)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, pk=None):
        movie = get_object_or_404(self.get_queryset(), id=pk)
        serializer = self.get_serializer(movie, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
